import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/home',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
    },
    {
      path: '/about',
      name: 'about',
      component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
      path: '/welcome',
      name: 'welcome',
      component: () => import(/* webpackChunkName: "welcome" */ '../views/Welcome.vue')
    },
    {
      path: '/inscription',
      name: 'inscription',
      component: () => import(/* webpackChunkName: "inscription" */ '../views/Inscription.vue')
    },
    {
      path: '/connexion',
      name: 'connexion',
      component: () => import(/* webpackChunkName: "login" */ '../views/Connexion.vue')
    }
  ]
})
