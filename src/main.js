// Import Vue
import Vue from 'vue'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'

import App from './App'
import router from './router'

Vue.use(Vuetify)


Vue.config.productionTip = false

// Import App Custom Styles
//import AppStyles from './assets/sass/main.scss'

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
